Arch Linux Install
// Check internet
```bash
ls /sys/firmware/efi/efivars
ping 8.8.8.8
timedatectl set-ntp true
```
// Create partitions
```bash
fdisk -l
gdisk /dev/sda    
cgdisk /dev/sda
```
// Format partitions
```bash
mkfs.fat -F32 /dev/sda1
mkswap /dev/sda2
swapon /dev/sda2
mkfs.ext4 /dev/sda3
mkfs.ext4 /dev/sda4
```
// Mount partitions
```bash
mount /dev/sda3 /mnt
mkdir -p /mnt/boot/efi
mkdir /mnt/home
mount /dev/sda1 /mnt/boot/efi
mount /dev/sda4 /mnt/home
```
// Rankmirrors for faster download 
```bash
pacman -S pacman-contrib
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup
rankmirrors -n 6 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist
```
// Install system
```bash
pacstrap -i /mnt base base-devel
genfstab -U /mnt >> /mnt/etc/fstab
```
// Configure system
```bash
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/America/Guayaquil /etc/localtime
hwclock --systohc
nano /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
echo myhostname > /etc/hostname
nano /etc/pacman.conf
passwd
useradd -m -g users -G wheel,storage,power -s /bin/bash username
passwd username
EDITOR=nano visudo
mkinitcpio -p linux
```
// Install grub
```bash
pacman -S grub efibootmgr bash-completion
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
pacman -S intel-ucode
grub-mkconfig -o /boot/grub/grub.cfg
```
// Install gnome
```bash
pacman -S mesa gdm gnome xorg-server xorg-apps xterm networkmanager gnome-tweak-tool 
systemctl enable gdm.service
systemctl enable NetworkManager.service
reboot
```
Aur helper
```bash
$ sudo pacman -S git
$ git clone https://aur.archlinux.org/yay.git
$ cd yay
$ makepkg -si
```
